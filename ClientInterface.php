<?php

namespace Officient\DocumentList;

use Officient\DocumentList\Exception\DocumentListException;

/**
 * Interface ClientInterface
 * @package Officient\DocumentList
 */
interface ClientInterface
{
    public const METHOD_GET = 'GET';
    public const METHOD_POST = 'POST';
    public const METHOD_PATCH = 'PATCH';
    public const METHOD_DELETE = 'DELETE';

    /**
     * @param string $query
     * @param null $data
     * @param string $method
     * @throws DocumentListException
     * @return Response
     */
    public function doRequest(string $query, $data = null, string $method = self::METHOD_GET): Response;
}