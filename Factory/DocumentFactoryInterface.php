<?php

namespace Officient\DocumentList\Factory;

use DateTimeInterface;
use Officient\DocumentList\Entity\Document;

/**
 * Interface DocumentFactoryInterface
 * @package Officient\DocumentList\Factory
 */
interface DocumentFactoryInterface
{
    /**
     * @param int|null $id
     * @param string|null $serviceTag
     * @param string|null $direction
     * @param string|null $directionTranslated
     * @param string|null $flowUuid
     * @param string|null $documentUuid
     * @param string|null $documentIdentifier
     * @param string|null $status
     * @param string|null $statusTranslated
     * @param string|null $userStatus
     * @param string|null $userStatusTranslated
     * @param int|null $userAssignee
     * @param DateTimeInterface|null $receivedDatetime
     * @param string|null $sender
     * @param string|null $receiver
     * @param string|null $receiverName
     * @param float|null $total
     * @param float|null $vatTotal
     * @param string|null $format
     * @param string|null $type
     * @param string|null $typeTranslated
     * @param string|null $ownerPrefix
     * @param int|null $ownerId
     * @param string|null $lastLogEntryType
     * @param string|null $lastLogEntryMessage
     * @param string|null $lastLogEntryMessageTranslated
     * @param DateTimeInterface|null $downloadedDatetime
     * @param DateTimeInterface|null $updatedDatetime
     * @param DateTimeInterface|null $createdDatetime
     * @param DateTimeInterface|null $deletedDatetime
     * @return Document
     */
    public function make(
        ?int $id,
        ?string $serviceTag,
        ?string $direction,
        ?string $directionTranslated,
        ?string $flowUuid,
        ?string $documentUuid,
        ?string $documentIdentifier,
        ?string $status,
        ?string $statusTranslated,
        ?string $userStatus,
        ?string $userStatusTranslated,
        ?int $userAssignee,
        ?DateTimeInterface $receivedDatetime,
        ?string $sender,
        ?string $receiver,
        ?string $receiverName,
        ?float $total,
        ?float $vatTotal,
        ?string $format,
        ?string $type,
        ?string $typeTranslated,
        ?string $ownerPrefix,
        ?int $ownerId,
        ?string $lastLogEntryType,
        ?string $lastLogEntryMessage,
        ?string $lastLogEntryMessageTranslated,
        ?DateTimeInterface $downloadedDatetime,
        ?DateTimeInterface $updatedDatetime,
        ?DateTimeInterface $createdDatetime,
        ?DateTimeInterface $deletedDatetime
    ): Document;
}