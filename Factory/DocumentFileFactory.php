<?php

namespace Officient\DocumentList\Factory;

use DateTimeInterface;
use Officient\DocumentList\Entity\DocumentFile;

class DocumentFileFactory implements DocumentFileFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function make(?int $id, ?string $tag, ?string $mimeType, ?string $originalName, ?string $diskReference, ?DateTimeInterface $createdDatetime): DocumentFile
    {
        return (new DocumentFile())
            ->setId($id)
            ->setTag($tag)
            ->setMimeType($mimeType)
            ->setOriginalName($originalName)
            ->setDiskReference($diskReference)
            ->setCreatedDatetime($createdDatetime);
    }
}