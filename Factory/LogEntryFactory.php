<?php

namespace Officient\DocumentList\Factory;

use DateTimeInterface;
use Officient\DocumentList\Entity\LogEntry;

/**
 * Class LogEntryFactory
 * @package Officient\DocumentList\Factory
 */
class LogEntryFactory implements LogEntryFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function make(
        ?int $id,
        ?string $message,
        ?string $messageTranslated,
        ?string $type,
        ?array $payload,
        ?string $status,
        ?string $statusTranslated,
        ?string $userStatus,
        ?string $userStatusTranslated,
        ?int $userAssignee,
        ?bool $muted,
        ?DateTimeInterface $createdDatetime
    ): LogEntry
    {
        return (new LogEntry())
            ->setId($id)
            ->setMessage($message)
            ->setMessageTranslated($messageTranslated)
            ->setType($type)
            ->setPayload($payload)
            ->setStatus($status)
            ->setStatusTranslated($statusTranslated)
            ->setUserStatus($userStatus)
            ->setUserStatusTranslated($userStatusTranslated)
            ->setUserAssignee($userAssignee)
            ->setMuted($muted)
            ->setCreatedDatetime($createdDatetime);
    }
}