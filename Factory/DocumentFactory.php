<?php

namespace Officient\DocumentList\Factory;

use DateTimeInterface;
use Officient\DocumentList\Entity\Document;

/**
 * Class DocumentFactory
 * @package Officient\DocumentList\Factory
 */
class DocumentFactory implements DocumentFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function make(
        ?int $id,
        ?string $serviceTag,
        ?string $direction,
        ?string $directionTranslated,
        ?string $flowUuid,
        ?string $documentUuid,
        ?string $documentIdentifier,
        ?string $status,
        ?string $statusTranslated,
        ?string $userStatus,
        ?string $userStatusTranslated,
        ?int $userAssignee,
        ?DateTimeInterface $receivedDatetime,
        ?string $sender,
        ?string $receiver,
        ?string $receiverName,
        ?float $total,
        ?float $vatTotal,
        ?string $format,
        ?string $type,
        ?string $typeTranslated,
        ?string $ownerPrefix,
        ?int $ownerId,
        ?string $lastLogEntryType,
        ?string $lastLogEntryMessage,
        ?string $lastLogEntryMessageTranslated,
        ?DateTimeInterface $downloadedDatetime,
        ?DateTimeInterface $updatedDatetime,
        ?DateTimeInterface $createdDatetime,
        ?DateTimeInterface $deletedDatetime
    ): Document
    {
        $document = new Document();
        $document->setId($id);
        $document->setServiceTag($serviceTag);
        $document->setDirection($direction);
        $document->setDirectionTranslated($directionTranslated);
        $document->setFlowUuid($flowUuid);
        $document->setDocumentUuid($documentUuid);
        $document->setDocumentIdentifier($documentIdentifier);
        $document->setStatus($status);
        $document->setStatusTranslated($statusTranslated);
        $document->setUserStatus($userStatus);
        $document->setUserStatusTranslated($userStatusTranslated);
        $document->setUserAssignee($userAssignee);
        $document->setReceivedDatetime($receivedDatetime);
        $document->setSender($sender);
        $document->setReceiver($receiver);
        $document->setReceiverName($receiverName);
        $document->setTotal($total);
        $document->setVatTotal($vatTotal);
        $document->setFormat($format);
        $document->setType($type);
        $document->setTypeTranslated($typeTranslated);
        $document->setOwnerPrefix($ownerPrefix);
        $document->setOwnerId($ownerId);
        $document->setLastLogEntryType($lastLogEntryType);
        $document->setLastLogEntryMessage($lastLogEntryMessage);
        $document->setLastLogEntryMessageTranslated($lastLogEntryMessageTranslated);
        $document->setDownloadedDatetime($downloadedDatetime);
        $document->setUpdatedDatetime($updatedDatetime);
        $document->setCreatedDatetime($createdDatetime);
        $document->setDeletedDatetime($deletedDatetime);

        return $document;
    }
}