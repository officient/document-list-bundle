<?php

namespace Officient\DocumentList\Factory;

use DateTimeInterface;
use Officient\DocumentList\Entity\DownloadEntry;

/**
 * Interface DownloadEntryFactoryInterface
 * @package Officient\DocumentList\Factory
 */
interface DownloadEntryFactoryInterface
{
    /**
     * @param int|null $id
     * @param string|null $ownerPrefix
     * @param int|null $ownerId
     * @param string|null $uuid
     * @param string|null $fileName
     * @param DateTimeInterface|null $expiresDatetime
     * @param DateTimeInterface|null $createdDatetime
     * @return DownloadEntry
     */
    public function make(
        ?int $id,
        ?string $ownerPrefix,
        ?int $ownerId,
        ?string $uuid,
        ?string $fileName,
        ?DateTimeInterface $expiresDatetime,
        ?DateTimeInterface $createdDatetime
    ): DownloadEntry;
}