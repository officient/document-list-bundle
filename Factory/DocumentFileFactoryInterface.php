<?php

namespace Officient\DocumentList\Factory;

use DateTimeInterface;
use Officient\DocumentList\Entity\DocumentFile;

/**
 * Interface DocumentFileFactoryInterface
 * @package Officient\DocumentList\Factory
 */
interface DocumentFileFactoryInterface
{
    /**
     * @param int|null $id
     * @param string|null $tag
     * @param string|null $mimeType
     * @param string|null $originalName
     * @param string|null $diskReference
     * @param DateTimeInterface|null $createdDatetime
     * @return DocumentFile
     */
    public function make(
        ?int $id,
        ?string $tag,
        ?string $mimeType,
        ?string $originalName,
        ?string $diskReference,
        ?DateTimeInterface $createdDatetime
    ): DocumentFile;
}