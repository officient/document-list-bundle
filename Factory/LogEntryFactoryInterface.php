<?php

namespace Officient\DocumentList\Factory;

use DateTimeInterface;
use Officient\DocumentList\Entity\LogEntry;

/**
 * Interface LogEntryFactoryInterface
 * @package Officient\DocumentList\Factory
 */
interface LogEntryFactoryInterface
{
    /**
     * @param int|null $id
     * @param string|null $message
     * @param string|null $messageTranslated
     * @param string|null $type
     * @param array|null $payload
     * @param string|null $status
     * @param string|null $statusTranslated
     * @param string|null $userStatus
     * @param string|null $userStatusTranslated
     * @param int|null $userAssignee
     * @param bool|null $muted
     * @param DateTimeInterface|null $createdDatetime
     * @return LogEntry
     */
    public function make(
        ?int $id,
        ?string $message,
        ?string $messageTranslated,
        ?string $type,
        ?array $payload,
        ?string $status,
        ?string $statusTranslated,
        ?string $userStatus,
        ?string $userStatusTranslated,
        ?int $userAssignee,
        ?bool $muted,
        ?DateTimeInterface $createdDatetime
    ): LogEntry;
}