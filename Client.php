<?php

namespace Officient\DocumentList;

use Officient\DocumentList\Exception\CouldNotConnectException;
use Officient\DocumentList\Exception\DocumentListException;

/**
 * Class Client
 *
 * This class handles all communication with Document List
 *
 * @package Officient\DocumentList
 */
class Client implements ClientInterface
{
    /**
     * @var string|null
     */
    private $host;

    /**
     * @var int|null
     */
    private $port;

    /**
     * Client constructor.
     * @param string|null $host
     * @param int|null $port
     */
    public function __construct(?string $host, ?int $port)
    {
        $this->host = $host;
        $this->port = $port;
    }

    /**
     * @inheritDoc
     */
    public function doRequest(string $query, $data = null, string $method = self::METHOD_GET): Response
    {
        if(empty($this->host)) {
            throw new CouldNotConnectException('Host is missing from config');
        }
        $handle = curl_init();
        $headers = [];

        curl_setopt($handle, CURLOPT_URL, $this->getHost().$query);
        if(!is_null($this->port)) {
            curl_setopt($handle, CURLOPT_PORT, $this->getPort());
        }
        curl_setopt($handle, CURLOPT_TIMEOUT, 30);
        curl_setopt($handle, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);
        if($method !== self::METHOD_GET) {
            if(!is_null($data)) {
                $postFields = json_encode($data);
                curl_setopt($handle, CURLOPT_POSTFIELDS, $postFields);
            }
        }
        curl_setopt($handle, CURLOPT_HEADERFUNCTION,
            function($curl, $header) use (&$headers)
            {
                $len = strlen($header);
                $header = explode(':', $header, 2);
                if (count($header) < 2) // ignore invalid headers
                    return $len;

                $headers[strtolower(trim($header[0]))][] = trim($header[1]);

                return $len;
            }
        );

        $content = curl_exec($handle);
        curl_close($handle);

        if(!$content) {
            throw new CouldNotConnectException("Could not connect to document list");
        }

        return new Response($content, $headers);
    }

    private function getHost()
    {
        $host = $this->host;
        if(substr($host, -1) !== '/') {
            $host .= '/';
        }
        return $host;
    }

    private function getPort()
    {
        return $this->port;
    }
}