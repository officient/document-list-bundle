<?php

namespace Officient\DocumentList\DependencyInjection;

use Officient\MasterData\Exception\MasterDataException;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * Class MasterDataExtension
 *
 * This class handles loading services and configurations
 *
 * @package Officient\DocumentList\DependencyInjection
 */
class DocumentListExtension extends Extension
{
    /**
     * @inheritDoc
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );
        $loader->load('services.yaml');

        $clientDef = $container->getDefinition('Officient\DocumentList\Client');
        $clientDef->replaceArgument(0, $config['api']['host'] ?? null);
        $clientDef->replaceArgument(1, $config['api']['port'] ?? null);
    }
}