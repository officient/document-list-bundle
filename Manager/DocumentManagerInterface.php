<?php

namespace Officient\DocumentList\Manager;

use DateTimeInterface;
use Officient\DocumentList\Entity\Document;
use Symfony\Component\HttpFoundation\Response;

/**
 * Interface DocumentManagerInterface
 * @package Officient\DocumentList\Manager
 */
interface DocumentManagerInterface
{
    /**
     * @param array|null $criteria
     * @param array|null $orderBy
     * @param int|null $limit
     * @param int|null $offset
     * @param string|null $locale
     * @param bool $includeDeleted
     * @return Document[]
     */
    public function paginateBy(?array $criteria = null, array $orderBy = null, ?int $limit = null, ?int $offset = null, ?string $locale = null, bool $includeDeleted = true): array;

    /**
     * @param array|null $criteria
     * @param int|null $limit
     * @param int|null $offset
     * @param bool $includeDeleted
     * @return int
     */
    public function countBy(?array $criteria = null, ?int $limit = null, ?int $offset = null, bool $includeDeleted = true): int;

    /**
     * @param array|null $criteria
     * @param array|null $orderBy
     * @param int|null $limit
     * @param int|null $offset
     * @param string|null $locale
     * @param bool $includeDeleted
     * @return Document
     */
    public function findBy(?array $criteria = null, array $orderBy = null, ?int $limit = null, ?int $offset = null, ?string $locale = null, bool $includeDeleted = true): array;

    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @param string|null $locale
     * @param bool $includeDeleted
     * @return Document|null
     */
    public function findOneBy(array $criteria, array $orderBy = null, ?string $locale = null, bool $includeDeleted = true): ?Document;

    /**
     * @param string $senderAddress
     * @param string $recipientAddress
     * @param string|null $senderName
     * @param string|null $recipientName
     * @param array|null $criteria
     * @param array|null $orderBy
     * @param int|null $limit
     * @param int|null $offset
     * @param string|null $locale
     * @param bool $includeDeleted
     * @return string|bool - string(hyperlink) if ready for download. True if queued. False if failed.
     */
    public function downloadMultiple(
        string $senderAddress,
        string $recipientAddress,
        ?string $senderName = null,
        ?string $recipientName = null,
        ?array $criteria = null,
        array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null,
        ?string $locale = null,
        bool $includeDeleted = true
    );

    /**
     * @param string $documentUuid
     * @param string $tag
     * @param bool $transform
     * @param bool $includeDeleted
     * @return Response
     */
    public function showContentByTagAndDocumentUuid(
        string $documentUuid,
        string $tag,
        bool $transform,
        bool $includeDeleted = true
    ): Response;

    /**
     * @param string $flowUuid
     * @param string $ownerPrefix
     * @param int $ownerId
     * @param DateTimeInterface $receivedDatetime
     * @param string|null $documentUuid
     * @param string|null $documentIdentifier
     * @param string|null $sender
     * @param string|null $receiver
     * @param string|null $receiverName
     * @param float|null $total
     * @param float|null $vatTotal
     * @param string|null $format
     * @param string|null $type
     * @param DateTimeInterface|null $downloadedDatetime
     * @param string|null $serviceTag
     * @param string|null $direction
     * @param DateTimeInterface|null $updatedDatetime
     * @param DateTimeInterface|null $createdDatetime
     * @return Document
     */
    public function store(
        string $flowUuid,
        string $ownerPrefix,
        int $ownerId,
        DateTimeInterface $receivedDatetime,
        ?string $documentUuid = null,
        ?string $documentIdentifier = null,
        ?string $sender = null,
        ?string $receiver = null,
        ?string $receiverName = null,
        ?float $total = null,
        ?float $vatTotal = null,
        ?string $format = null,
        ?string $type = null,
        ?DateTimeInterface $downloadedDatetime = null,
        ?string $serviceTag = null,
        ?string $direction = null,
        ?DateTimeInterface $updatedDatetime = null,
        ?DateTimeInterface $createdDatetime = null
    ): Document;

    /**
     * @param string $flowUuid
     * @param string $documentUuid
     * @return bool
     */
    public function destroy(string $flowUuid, string $documentUuid): bool;

    /**
     * @param DateTimeInterface $date
     * @param string|null $ownerPrefix
     * @param array|null $ownerIds
     * @param array|null $serviceTags
     * @param bool $includeDeleted
     * @return array
     */
    public function countByDateGrouped(
        DateTimeInterface $date,
        ?string $ownerPrefix = null,
        ?array $ownerIds = null,
        ?array $serviceTags = null,
        bool $includeDeleted = true
    ): array;

    /**
     * @param string $ownerPrefix
     * @param string $ownerId
     * @param string $serviceTag
     * @param DateTimeInterface $from
     * @param DateTimeInterface $to
     * @param array $formats
     * @param array $types
     * @param array $directions
     * @param bool $includeDeleted
     * @return int
     */
    public function countForInvoicing(
        string $ownerPrefix,
        string $ownerId,
        string $serviceTag,
        DateTimeInterface $from,
        DateTimeInterface $to,
        array $formats,
        array $types,
        array $directions,
        bool $includeDeleted = true
    ): int;
}