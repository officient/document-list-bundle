<?php

namespace Officient\DocumentList\Manager;

use Officient\DocumentList\ClientInterface;

/**
 * Class AbstractManager
 *
 * All managers should extend this class
 * A manager should handle the data flow between
 * the application and Document List.
 *
 * @package Officient\DocumentList\Manager
 */
abstract class AbstractManager
{
    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * Manager constructor.
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }
}