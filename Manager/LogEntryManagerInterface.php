<?php

namespace Officient\DocumentList\Manager;

use Officient\DocumentList\Entity\Document;
use Officient\DocumentList\Entity\LogEntry;

/**
 * Interface LogEntryManagerInterface
 * @package Officient\DocumentList\Manager
 */
interface LogEntryManagerInterface
{
    /**
     * @param Document $document
     * @param array|null $criteria
     * @param int|null $limit
     * @param int|null $offset
     * @return int
     */
    public function countBy(Document $document, ?array $criteria = null, ?int $limit = null, ?int $offset = null): int;

    /**
     * @param Document $document
     * @param array|null $criteria
     * @param array|null $orderBy
     * @param int|null $limit
     * @param int|null $offset
     * @param string|null $locale
     * @return LogEntry[]
     */
    public function findBy(Document $document, ?array $criteria = null, array $orderBy = null, ?int $limit = null, ?int $offset = null, ?string $locale = null): array;

    /**
     * @param Document $document
     * @param array $criteria
     * @param array|null $orderBy
     * @param string|null $locale
     * @return LogEntry|null
     */
    public function findOneBy(Document $document, array $criteria, array $orderBy = null, ?string $locale = null): ?LogEntry;

    /**
     * @param Document $document
     * @param string $message
     * @param string $type
     * @param string|null $status
     * @param string|null $userStatus
     * @param int|null $userAssignee
     * @param array|null $payload
     * @param bool $muted
     * @return LogEntry
     */
    public function store(Document $document, string $message, string $type, ?string $status, ?string $userStatus, ?int $userAssignee, ?array $payload = null, bool $muted = false): LogEntry;

    /**
     * @param string $flowUuid
     * @param string $documentUuid
     * @param string $message
     * @param string $type
     * @param string|null $status
     * @param string|null $userStatus
     * @param int|null $userAssignee
     * @param array|null $payload
     * @param bool $muted
     * @return LogEntry
     */
    public function storeFlow(string $flowUuid, string $documentUuid, string $message, string $type, ?string $status, ?string $userStatus, ?int $userAssignee,?array $payload = null, bool $muted = false): LogEntry;
}