<?php

namespace Officient\DocumentList\Manager;

use Officient\DocumentList\Client;

/**
 * Class TranslationManager
 * @package Officient\DocumentList\Manager
 */
class TranslationManager extends AbstractManager implements TranslationManagerInterface
{
    /**
     * @inheritDoc
     */
    public function getMessages(?string $locale = null): array
    {
        $url = 'translations/log_entry/messages';
        if($locale) {
            $url .= '?locale='.$locale;
        }

        $response = $this->client->doRequest($url, null, Client::METHOD_GET);

        $result = $response->getResult();
        return json_decode(json_encode($result), true);
    }

    /**
     * @inheritDoc
     */
    public function getStatuses(?string $locale = null): array
    {
        $url = 'translations/log_entry/statuses';
        if($locale) {
            $url .= '?locale='.$locale;
        }

        $response = $this->client->doRequest($url, null, Client::METHOD_GET);

        $result = $response->getResult();
        return json_decode(json_encode($result), true);
    }

    /**
     * @inheritDoc
     */
    public function getUserStatuses(?string $locale = null): array
    {
        $url = 'translations/log_entry/user_statuses';
        if($locale) {
            $url .= '?locale='.$locale;
        }

        $response = $this->client->doRequest($url, null, Client::METHOD_GET);

        $result = $response->getResult();
        return json_decode(json_encode($result), true);
    }

    /**
     * @inheritDoc
     */
    public function getTypes(?string $locale = null): array
    {
        $url = 'translations/document/types';
        if($locale) {
            $url .= '?locale='.$locale;
        }

        $response = $this->client->doRequest($url, null, Client::METHOD_GET);

        $result = $response->getResult();
        return json_decode(json_encode($result), true);
    }

    /**
     * @inheritDoc
     */
    public function getDirections(?string $locale = null): array
    {
        $url = 'translations/document/directions';
        if($locale) {
            $url .= '?locale='.$locale;
        }

        $response = $this->client->doRequest($url, null, Client::METHOD_GET);

        $result = $response->getResult();
        return json_decode(json_encode($result), true);
    }
}