<?php

namespace Officient\DocumentList\Manager;

use Officient\DocumentList\Client;
use Officient\DocumentList\ClientInterface;
use Officient\DocumentList\Entity\Document;
use Officient\DocumentList\Entity\DocumentFile;
use Officient\DocumentList\Exception\NonUniqueResultException;
use Officient\DocumentList\Exception\NoResultException;
use Officient\DocumentList\Factory\DocumentFileFactoryInterface;
use Symfony\Component\HttpFoundation\Response;

class DocumentFileManager extends AbstractManager implements DocumentFileManagerInterface
{
    /**
     * @var DocumentFileFactoryInterface
     */
    private $factory;

    /**
     * @inheritDoc
     */
    public function __construct(ClientInterface $client, DocumentFileFactoryInterface $factory)
    {
        parent::__construct($client);
        $this->factory = $factory;
    }

    /**
     * @inheritDoc
     */
    public function countBy(Document $document, ?array $criteria = null, ?int $limit = null, ?int $offset = null): int
    {
        $response = $this->client->doRequest('documents/'.$document->getId().'/files/count_by', [
            'criteria' => $criteria,
            'limit' => $limit,
            'offset' => $offset
        ], Client::METHOD_POST);

        return $response->getResult();
    }

    /**
     * @inheritDoc
     */
    public function findBy(Document $document, ?array $criteria = null, array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        $response = $this->client->doRequest('documents/'.$document->getId().'/files/find_by', [
            'criteria' => $criteria,
            'order_by' => $orderBy,
            'limit' => $limit,
            'offset' => $offset,
        ], Client::METHOD_POST);

        $result = array();
        foreach($response->getResult() as $record) {
            $createdDatetime = null;
            if (is_null($record->createdDatetime) === false) {
                $createdDatetime = new \DateTime(
                    $record->createdDatetime->date,
                    new \DateTimeZone($record->createdDatetime->timezone)
                );
            }

            $result[] = $this->factory->make(
                $record->id,
                $record->tag,
                $record->mimeType,
                $record->originalName,
                $record->diskReference,
                $createdDatetime
            );
        }

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function findOneBy(Document $document, array $criteria, array $orderBy = null): ?DocumentFile
    {
        try {
            $response = $this->client->doRequest('documents/' . $document->getId() . '/files/find_one_by', [
                'criteria' => $criteria,
                'order_by' => $orderBy
            ], Client::METHOD_POST);

            $record = $response->getResult();
            if (is_null($record) === false) {
                $createdDatetime = null;
                if (is_null($record->createdDatetime) === false) {
                    $createdDatetime = new \DateTime(
                        $record->createdDatetime->date,
                        new \DateTimeZone($record->createdDatetime->timezone)
                    );
                }

                return $this->factory->make(
                    $record->id,
                    $record->tag,
                    $record->mimeType,
                    $record->originalName,
                    $record->diskReference,
                    $createdDatetime
                );
            } else {
                return null;
            }
        } catch (NonUniqueResultException | NoResultException $exception) {
            return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function findLatestOneByTag(Document $document, string $tag): ?DocumentFile
    {
        try {
            $response = $this->client->doRequest('documents/' . $document->getId() . '/files/find_latest_one_by_tag', [
                'tag' => $tag
            ], Client::METHOD_POST);

            $record = $response->getResult();
            if (is_null($record) === false) {
                $createdDatetime = null;
                if (is_null($record->createdDatetime) === false) {
                    $createdDatetime = new \DateTime(
                        $record->createdDatetime->date,
                        new \DateTimeZone($record->createdDatetime->timezone)
                    );
                }

                return $this->factory->make(
                    $record->id,
                    $record->tag,
                    $record->mimeType,
                    $record->originalName,
                    $record->diskReference,
                    $createdDatetime
                );
            } else {
                return null;
            }
        } catch (NonUniqueResultException | NoResultException $exception) {
            return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function findLatestContentByTag(?Document $document, ?string $flowUuid, ?string $documentUuid, string $tag, bool $decode = true): ?string
    {
        if(is_null($document) && (is_null($flowUuid) || is_null($documentUuid))) {
            throw new \RuntimeException("Either document or flowUuid and documentUuid should be provided");
        }

        try {
            if(!is_null($document)) {
                $response = $this->client->doRequest('documents/' . $document->getId() . '/files/find_latest_content_by_tag', [
                    'tag' => $tag
                ], Client::METHOD_POST);
            } else if(!is_null($flowUuid) && !is_null($documentUuid)) {
                $response = $this->client->doRequest('documents/' . $flowUuid . '/' . $documentUuid . '/files/find_latest_content_by_tag', [
                    'tag' => $tag
                ], Client::METHOD_POST);
            }

            $string = $response->getResult();
            if($string) {
                if($decode) {
                    return base64_decode($string);
                } else {
                    return $string;
                }
            } else {
                return null;
            }
        } catch (NonUniqueResultException | NoResultException $exception) {
            return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function getContent(Document $document, DocumentFile $documentFile, bool $decode = true): ?string
    {
        try {
            $response = $this->client->doRequest('documents/' . $document->getId() . '/files/'.$documentFile->getId(), null, Client::METHOD_GET);
            $string = $response->getResult();
            if($string) {
                if($decode) {
                    return base64_decode($string);
                } else {
                    return $string;
                }
            } else {
                return null;
            }
        } catch (NonUniqueResultException | NoResultException $exception) {
            return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function showContent(Document $document, DocumentFile $documentFile, bool $transform = false): Response
    {
        $response = $this->client->doRequest('documents/' . $document->getId() . '/files/'.$documentFile->getId() .'/show?transform='.(int)$transform, null, Client::METHOD_GET);
        $headers = $response->getHeaders();
        $contentType = $headers['content-type'] ?? null;
        $rawContent = $response->getRawContent();
        return new Response($rawContent, 200, ['Content-Type' => $contentType]);
    }

    /**
     * @inheritDoc
     */
    public function downloadContent(Document $document, DocumentFile $documentFile): Response
    {
        $response = $this->client->doRequest('documents/' . $document->getId() . '/files/'.$documentFile->getId() .'/download', null, Client::METHOD_GET);
        $headers = $response->getHeaders();
        $contentType = $headers['content-type'] ?? null;
        $contentDisposition = $headers['content-disposition'] ?? null;
        $rawContent = $response->getRawContent();
        return new Response($rawContent, 200, ['Content-Type' => $contentType, 'Content-Disposition' => $contentDisposition]);
    }

    /**
     * @inheritDoc
     */
    public function store(Document $document, string $data, ?string $tag = null, ?string $originalName = null, ?string $md5Checksum = null): DocumentFile
    {
        $data = [
            'base64_data' => base64_encode($data),
            'tag' => $tag,
            'original_name' => $originalName,
            'md5_checksum' => $md5Checksum
        ];

        $response = $this->client->doRequest('documents/'.$document->getId().'/files', $data, Client::METHOD_POST);
        $record = $response->getResult();

        $createdDatetime = null;
        if (is_null($record->createdDatetime) === false) {
            $createdDatetime = new \DateTime(
                $record->createdDatetime->date,
                new \DateTimeZone($record->createdDatetime->timezone)
            );
        }

        return $this->factory->make(
            $record->id,
            $record->tag,
            $record->mimeType,
            $record->originalName,
            $record->diskReference,
            $createdDatetime
        );
    }
}