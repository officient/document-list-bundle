<?php

namespace Officient\DocumentList\Manager;

use Officient\DocumentList\Client;
use Officient\DocumentList\ClientInterface;
use Officient\DocumentList\Entity\Document;
use Officient\DocumentList\Entity\LogEntry;
use Officient\DocumentList\Exception\NonUniqueResultException;
use Officient\DocumentList\Exception\NoResultException;
use Officient\DocumentList\Factory\LogEntryFactoryInterface;

/**
 * Class LogEntryManager
 * @package Officient\DocumentList\Manager
 */
class LogEntryManager extends AbstractManager implements LogEntryManagerInterface
{
    /**
     * @var LogEntryFactoryInterface
     */
    private $factory;

    /**
     * @inheritDoc
     */
    public function __construct(ClientInterface $client, LogEntryFactoryInterface $factory)
    {
        parent::__construct($client);
        $this->factory = $factory;
    }

    /**
     * @inheritDoc
     */
    public function countBy(Document $document, ?array $criteria = null, ?int $limit = null, ?int $offset = null): int
    {
        $response = $this->client->doRequest('documents/'.$document->getId().'/log_entries/count_by', [
            'criteria' => $criteria,
            'limit' => $limit,
            'offset' => $offset
        ], Client::METHOD_POST);

        return $response->getResult();
    }

    /**
     * @inheritDoc
     */
    public function findBy(Document $document, ?array $criteria = null, array $orderBy = null, ?int $limit = null, ?int $offset = null, ?string $locale = null): array
    {
        $params = [
            'criteria' => $criteria,
            'order_by' => $orderBy,
            'limit' => $limit,
            'offset' => $offset
        ];
        if($locale) {
             $params['locale'] = $locale;
        }
        $response = $this->client->doRequest('documents/'.$document->getId().'/log_entries/find_by', $params, Client::METHOD_POST);

        $result = array();
        foreach($response->getResult() as $record) {
            $createdDatetime = null;
            if (is_null($record->createdDatetime) === false) {
                $createdDatetime = new \DateTime(
                    $record->createdDatetime->date,
                    new \DateTimeZone($record->createdDatetime->timezone)
                );
            }

            $result[] = $this->factory->make(
                $record->id,
                $record->message,
                $record->messageTranslated,
                $record->type,
                ($record->payload ? (array)$record->payload : null),
                $record->status,
                $record->statusTranslated,
                $record->userStatus,
                $record->userStatusTranslated,
                $record->userAssignee,
                $record->muted,
                $createdDatetime
            );
        }

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function findOneBy(Document $document, array $criteria, array $orderBy = null, ?string $locale = null): ?LogEntry
    {
        try {
            $params = [
                'criteria' => $criteria,
                'order_by' => $orderBy
            ];
            if($locale) {
                $params['locale'] = $locale;
            }
            $response = $this->client->doRequest('documents/'.$document->getId().'/log_entries/find_one_by', $params, Client::METHOD_POST);

            $record = $response->getResult();
            if(is_null($record) === false) {
                $createdDatetime = null;
                if (is_null($record->createdDatetime) === false) {
                    $createdDatetime = new \DateTime(
                        $record->createdDatetime->date,
                        new \DateTimeZone($record->createdDatetime->timezone)
                    );
                }

                return $this->factory->make(
                    $record->id,
                    $record->message,
                    $record->messageTranslated,
                    $record->type,
                    ($record->payload ? (array)$record->payload : null),
                    $record->status,
                    $record->statusTranslated,
                    $record->userStatus,
                    $record->userStatusTranslated,
                    $record->userAssignee,
                    $record->muted,
                    $createdDatetime
                );
            }
        } catch (NonUniqueResultException | NoResultException $exception) {
            return null;
        }
        return null;
    }

    /**
     * @inheritDoc
     */
    public function store(Document $document, string $message, string $type, ?string $status, ?string $userStatus, ?int $userAssignee,?array $payload = null, bool $muted = false): LogEntry
    {
        $data = [
            'message' => $message,
            'type' => $type
        ];

        if($status) {
            $data['status'] = $status;
        }

        if($userStatus) {
            $data['user_status'] = $userStatus;
        }

        if($userAssignee) {
            $data['user_assignee'] = $userAssignee;
        }

        if($payload) {
            $data['payload'] = $payload;
        }

        $data['muted'] = $muted;

        $response = $this->client->doRequest("documents/".$document->getId().'/log_entries', $data, Client::METHOD_POST);
        $record = $response->getResult();

        $createdDatetime = null;
        if (is_null($record->createdDatetime) === false) {
            $createdDatetime = new \DateTime(
                $record->createdDatetime->date,
                new \DateTimeZone($record->createdDatetime->timezone)
            );
        }

        return $this->factory->make(
            $record->id,
            $record->message,
            $record->messageTranslated,
            $record->type,
            ($record->payload ? (array)$record->payload : null),
            $record->status,
            $record->statusTranslated,
            $record->userStatus,
            $record->userStatusTranslated,
            $record->userAssignee,
            $record->muted,
            $createdDatetime
        );
    }

    /**
     * @inheritDoc
     */
    public function storeFlow(string $flowUuid, string $documentUuid, string $message, string $type, ?string $status, ?string $userStatus, ?int $userAssignee,?array $payload = null, bool $muted = false): LogEntry
    {
        $data = [
            'message' => $message,
            'type' => $type
        ];

        if($status) {
            $data['status'] = $status;
        }

        if($userStatus) {
            $data['user_status'] = $userStatus;
        }

        if($userAssignee) {
            $data['user_assignee'] = $userAssignee;
        }

        if($payload) {
            $data['payload'] = $payload;
        }

        $data['muted'] = $muted;

        $response = $this->client->doRequest("documents/".$flowUuid.'/'.$documentUuid.'/log_entries', $data, Client::METHOD_POST);
        $record = $response->getResult();

        $createdDatetime = null;
        if (is_null($record->createdDatetime) === false) {
            $createdDatetime = new \DateTime(
                $record->createdDatetime->date,
                new \DateTimeZone($record->createdDatetime->timezone)
            );
        }

        return $this->factory->make(
            $record->id,
            $record->message,
            $record->messageTranslated,
            $record->type,
            ($record->payload ? (array)$record->payload : null),
            $record->status,
            $record->statusTranslated,
            $record->userStatus,
            $record->userStatusTranslated,
            $record->userAssignee,
            $record->muted,
            $createdDatetime
        );
    }
}