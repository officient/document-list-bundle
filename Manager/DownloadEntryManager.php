<?php

namespace Officient\DocumentList\Manager;

use Officient\DocumentList\Client;
use Officient\DocumentList\ClientInterface;
use Officient\DocumentList\Entity\DownloadEntry;
use Officient\DocumentList\Exception\NonUniqueResultException;
use Officient\DocumentList\Exception\NoResultException;
use Officient\DocumentList\Factory\DownloadEntryFactoryInterface;
use Symfony\Component\HttpFoundation\Response;

class DownloadEntryManager extends AbstractManager implements DownloadEntryManagerInterface
{
    /**
     * @var DownloadEntryFactoryInterface
     */
    private $factory;

    public function __construct(ClientInterface $client, DownloadEntryFactoryInterface $factory)
    {
        parent::__construct($client);
        $this->factory = $factory;
    }

    public function countBy(?array $criteria = null, ?int $limit = null, ?int $offset = null): int
    {
        $response = $this->client->doRequest('download_entries/count_by', [
            'criteria' => $criteria,
            'limit' => $limit,
            'offset' => $offset
        ], Client::METHOD_POST);

        return $response->getResult();
    }

    public function findBy(?array $criteria = null, array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        $response = $this->client->doRequest('download_entries/find_by', [
            'criteria' => $criteria,
            'order_by' => $orderBy,
            'limit' => $limit,
            'offset' => $offset
        ], Client::METHOD_POST);

        $result = array();
        foreach($response->getResult() as $record) {
            $expiresDatetime = null;
            if (is_null($record->expiresDatetime) === false) {
                $expiresDatetime = new \DateTime(
                    $record->expiresDatetime->date,
                    new \DateTimeZone($record->expiresDatetime->timezone)
                );
            }

            $createdDatetime = null;
            if (is_null($record->createdDatetime) === false) {
                $createdDatetime = new \DateTime(
                    $record->createdDatetime->date,
                    new \DateTimeZone($record->createdDatetime->timezone)
                );
            }

            $result[] = $this->factory->make(
                $record->id,
                $record->ownerPrefix,
                $record->ownerId,
                $record->uuid,
                $record->fileName,
                $expiresDatetime,
                $createdDatetime
            );
        }

        return $result;
    }

    public function findOneBy(array $criteria, array $orderBy = null): ?DownloadEntry
    {
        try {
            $response = $this->client->doRequest('download_entries/find_one_by', [
                'criteria' => $criteria,
                'order_by' => $orderBy
            ], Client::METHOD_POST);

            $record = $response->getResult();
            if (is_null($record) === false) {
                $expiresDatetime = null;
                if (is_null($record->expiresDatetime) === false) {
                    $expiresDatetime = new \DateTime(
                        $record->expiresDatetime->date,
                        new \DateTimeZone($record->expiresDatetime->timezone)
                    );
                }

                $createdDatetime = null;
                if (is_null($record->createdDatetime) === false) {
                    $createdDatetime = new \DateTime(
                        $record->createdDatetime->date,
                        new \DateTimeZone($record->createdDatetime->timezone)
                    );
                }

                return $this->factory->make(
                    $record->id,
                    $record->ownerPrefix,
                    $record->ownerId,
                    $record->uuid,
                    $record->fileName,
                    $expiresDatetime,
                    $createdDatetime
                );
            } else {
                return null;
            }
        } catch (NonUniqueResultException | NoResultException $exception) {
            return null;
        }
    }

    public function queue(
        string $ownerPrefix,
        int $ownerId,
        string $senderAddress,
        string $recipientAddress,
        ?string $senderName = null,
        ?string $recipientName = null,
        ?array $criteria = null,
        ?array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null,
        bool $includeDeleted = true,
        ?string $locale = null
    ): void
    {
        $this->client->doRequest('download_entries/queue', [
            'owner_prefix' => $ownerPrefix,
            'owner_id' => $ownerId,
            'sender_address' => $senderAddress,
            'sender_name' => $senderName,
            'recipient_address' => $recipientAddress,
            'recipient_name' => $recipientName,
            'criteria' => $criteria,
            'order_by' => $orderBy,
            'limit' => $limit,
            'offset' => $offset,
            'include_deleted' => $includeDeleted,
            'locale' => $locale
        ], Client::METHOD_POST);
    }

    public function getContent(DownloadEntry $downloadEntry, bool $base64Decode = false): string|false
    {
        $response = $this->client->doRequest('download_entries/'.$downloadEntry->getId().'/get_content');
        $content = $response->getResult();

        if($base64Decode) {
            $content = base64_decode($content);
        }

        return $content;
    }

    public function downloadContent(DownloadEntry $downloadEntry): Response
    {
        $content = $this->getContent($downloadEntry, true);
        return new Response($content, Response::HTTP_OK, [
            'Content-Type' => 'application/zip',
            'Content-Disposition' => 'attachment; filename="'.$downloadEntry->getFileName().'"',
            'Content-Length' => strlen($content)
        ]);
    }
}