<?php

namespace Officient\DocumentList\Manager;

use DateTimeInterface;
use Officient\DocumentList\Client;
use Officient\DocumentList\ClientInterface;
use Officient\DocumentList\Entity\Document;
use Officient\DocumentList\Exception\DocumentListException;
use Officient\DocumentList\Exception\NonUniqueResultException;
use Officient\DocumentList\Exception\NoResultException;
use Officient\DocumentList\Factory\DocumentFactoryInterface;
use Officient\DocumentList\Response;

/**
 * Class DocumentManager
 * @package Officient\DocumentList\Manager
 */
class DocumentManager extends AbstractManager implements DocumentManagerInterface
{
    /**
     * @var DocumentFactoryInterface
     */
    private $factory;

    /**
     * @inheritDoc
     */
    public function __construct(ClientInterface $client, DocumentFactoryInterface  $factory)
    {
        parent::__construct($client);
        $this->factory = $factory;
    }

    /**
     * @inheritDoc
     * @throws DocumentListException
     */
    public function paginateBy(?array $criteria = null, array $orderBy = null, ?int $limit = null, ?int $offset = null, ?string $locale = null, bool $includeDeleted = true): array
    {
        $params = [
            'criteria' => $criteria,
            'order_by' => $orderBy,
            'limit' => $limit,
            'offset' => $offset,
            'include_deleted' => $includeDeleted
        ];
        if($locale) {
            $params['locale'] = $locale;
        }
        $response = $this->client->doRequest('documents/paginate_by', $params, Client::METHOD_POST);

        $result = array();
        foreach($response->getResult() as $record) {
            $receivedDatetime = null;
            if (is_null($record->receivedDatetime) === false) {
                $receivedDatetime = new \DateTime(
                    $record->receivedDatetime->date,
                    new \DateTimeZone($record->receivedDatetime->timezone)
                );
            }
            $updatedDatetime = null;
            if (is_null($record->updatedDatetime) === false) {
                $updatedDatetime = new \DateTime(
                    $record->updatedDatetime->date,
                    new \DateTimeZone($record->updatedDatetime->timezone)
                );
            }
            $createdDatetime = null;
            if (is_null($record->createdDatetime) === false) {
                $createdDatetime = new \DateTime(
                    $record->createdDatetime->date,
                    new \DateTimeZone($record->createdDatetime->timezone)
                );
            }
            $deletedDatetime = null;
            if (is_null($record->deletedDatetime) === false) {
                $deletedDatetime = new \DateTime(
                    $record->deletedDatetime->date,
                    new \DateTimeZone($record->deletedDatetime->timezone)
                );
            }

            $result[] = $this->factory->make(
                $record->id,
                $record->serviceTag,
                $record->direction,
                $record->directionTranslated,
                $record->flowUuid,
                $record->documentUuid,
                $record->documentIdentifier,
                $record->status,
                $record->statusTranslated,
                $record->userStatus,
                $record->userStatusTranslated,
                $record->userAssignee,
                $receivedDatetime,
                $record->sender,
                $record->receiver,
                $record->receiverName,
                $record->total,
                $record->vatTotal,
                $record->format,
                $record->type,
                $record->typeTranslated,
                $record->ownerPrefix,
                $record->ownerId,
                $record->lastLogEntryType,
                $record->lastLogEntryMessage,
                $record->lastLogEntryMessageTranslated,
                is_null($record->downloadedDatetime) ? null : new \DateTime($record->downloadedDatetime->date, new \DateTimeZone($record->downloadedDatetime->timezone)),
                $updatedDatetime,
                $createdDatetime,
                $deletedDatetime
            );
        }
        return $result;
    }

    /**
     * @inheritDoc
     * @throws DocumentListException
     */
    public function countBy(?array $criteria = null, ?int $limit = null, ?int $offset = null, bool $includeDeleted = true): int
    {
        $response = $this->client->doRequest('documents/count_by', [
            'criteria' => $criteria,
            'limit' => $limit,
            'offset' => $offset,
            'include_deleted' => $includeDeleted
        ], Client::METHOD_POST);

        return $response->getResult();
    }

    /**
     * @inheritDoc
     * @throws DocumentListException
     */
    public function findBy(?array $criteria = null, array $orderBy = null, ?int $limit = null, ?int $offset = null, ?string $locale = null, bool $includeDeleted = true): array
    {
        $params = [
            'criteria' => $criteria,
            'order_by' => $orderBy,
            'limit' => $limit,
            'offset' => $offset,
            'include_deleted' => $includeDeleted
        ];
        if($locale) {
            $params['locale'] = $locale;
        }
        $response = $this->client->doRequest('documents/find_by', $params, Client::METHOD_POST);

        $result = array();
        foreach($response->getResult() as $record) {
            $receivedDatetime = null;
            if (is_null($record->receivedDatetime) === false) {
                $receivedDatetime = new \DateTime(
                    $record->receivedDatetime->date,
                    new \DateTimeZone($record->receivedDatetime->timezone)
                );
            }
            $updatedDatetime = null;
            if (is_null($record->updatedDatetime) === false) {
                $updatedDatetime = new \DateTime(
                    $record->updatedDatetime->date,
                    new \DateTimeZone($record->updatedDatetime->timezone)
                );
            }
            $createdDatetime = null;
            if (is_null($record->createdDatetime) === false) {
                $createdDatetime = new \DateTime(
                    $record->createdDatetime->date,
                    new \DateTimeZone($record->createdDatetime->timezone)
                );
            }
            $deletedDatetime = null;
            if (is_null($record->deletedDatetime) === false) {
                $deletedDatetime = new \DateTime(
                    $record->deletedDatetime->date,
                    new \DateTimeZone($record->deletedDatetime->timezone)
                );
            }

            $result[] = $this->factory->make(
                $record->id,
                $record->serviceTag,
                $record->direction,
                $record->directionTranslated,
                $record->flowUuid,
                $record->documentUuid,
                $record->documentIdentifier,
                $record->status,
                $record->statusTranslated,
                $record->userStatus,
                $record->userStatusTranslated,
                $record->userAssignee,
                $receivedDatetime,
                $record->sender,
                $record->receiver,
                $record->receiverName,
                $record->total,
                $record->vatTotal,
                $record->format,
                $record->type,
                $record->typeTranslated,
                $record->ownerPrefix,
                $record->ownerId,
                $record->lastLogEntryType,
                $record->lastLogEntryMessage,
                $record->lastLogEntryMessageTranslated,
                is_null($record->downloadedDatetime) ? null : new \DateTime($record->downloadedDatetime->date, new \DateTimeZone($record->downloadedDatetime->timezone)),
                $updatedDatetime,
                $createdDatetime,
                $deletedDatetime
            );
        }
        return $result;
    }

    /**
     * @inheritDoc
     * @throws DocumentListException
     */
    public function findOneBy(array $criteria, array $orderBy = null, ?string $locale = null, bool $includeDeleted = true): ?Document
    {
        try {
            $params = [
                'criteria' => $criteria,
                'order_by' => $orderBy,
                'include_deleted' => $includeDeleted
            ];
            if($locale) {
                $params['locale'] = $locale;
            }
            $response = $this->client->doRequest('documents/find_one_by', $params, Client::METHOD_POST);

            $record = $response->getResult();
            if (is_null($record) === false) {
                $receivedDatetime = null;
                if (is_null($record->receivedDatetime) === false) {
                    $receivedDatetime = new \DateTime(
                        $record->receivedDatetime->date,
                        new \DateTimeZone($record->receivedDatetime->timezone)
                    );
                }
                $updatedDatetime = null;
                if (is_null($record->updatedDatetime) === false) {
                    $updatedDatetime = new \DateTime(
                        $record->updatedDatetime->date,
                        new \DateTimeZone($record->updatedDatetime->timezone)
                    );
                }
                $createdDatetime = null;
                if (is_null($record->createdDatetime) === false) {
                    $createdDatetime = new \DateTime(
                        $record->createdDatetime->date,
                        new \DateTimeZone($record->createdDatetime->timezone)
                    );
                }
                $deletedDatetime = null;
                if (is_null($record->deletedDatetime) === false) {
                    $deletedDatetime = new \DateTime(
                        $record->deletedDatetime->date,
                        new \DateTimeZone($record->deletedDatetime->timezone)
                    );
                }

                return $this->factory->make(
                    $record->id,
                    $record->serviceTag,
                    $record->direction,
                    $record->directionTranslated,
                    $record->flowUuid,
                    $record->documentUuid,
                    $record->documentIdentifier,
                    $record->status,
                    $record->statusTranslated,
                    $record->userStatus,
                    $record->userStatusTranslated,
                    $record->userAssignee,
                    $receivedDatetime,
                    $record->sender,
                    $record->receiver,
                    $record->receiverName,
                    $record->total,
                    $record->vatTotal,
                    $record->format,
                    $record->type,
                    $record->typeTranslated,
                    $record->ownerPrefix,
                    $record->ownerId,
                    $record->lastLogEntryType,
                    $record->lastLogEntryMessage,
                    $record->lastLogEntryMessageTranslated,
                    is_null($record->downloadedDatetime) ? null : new \DateTime($record->downloadedDatetime->date, new \DateTimeZone($record->downloadedDatetime->timezone)),
                    $updatedDatetime,
                    $createdDatetime,
                    $deletedDatetime
                );
            }
        } catch (NonUniqueResultException | NoResultException $exception) {
            return null;
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public function downloadMultiple(
        string $senderAddress,
        string $recipientAddress,
        ?string $senderName = null,
        ?string $recipientName = null,
        ?array $criteria = null,
        array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null,
        ?string $locale = null,
        bool $includeDeleted = true
    )
    {
        $params = [
            'criteria' => $criteria,
            'order_by' => $orderBy,
            'limit' => $limit,
            'offset' => $offset,
            'locale' => $locale,
            'sender_address' => $senderAddress,
            'sender_name' => $senderName,
            'recipient_address' => $recipientAddress,
            'recipient_name' => $recipientName,
            'include_deleted' => $includeDeleted
        ];

        $response = $this->client->doRequest('documents/download_multiple', $params, Client::METHOD_POST);
        $type = $response->getResult()->type ?? null;

        switch ($type) {
            case 'queued':
                return true;
            case 'ready':
                $link = $response->getResult()->link ?? null;
                return is_null($link) ? null : $link;
            default:
                return false;
        }
    }

    /**
     * @inheritDoc
     */
    public function showContentByTagAndDocumentUuid(
        string $documentUuid,
        string $tag,
        bool $transform,
        bool $includeDeleted = true
    ): \Symfony\Component\HttpFoundation\Response
    {
        $response = $this->client->doRequest('documents/'.$documentUuid.'/'.$tag.'/show_content_by_tag_and_document_uuid?transform='.(int)$transform.'&include_deleted='.(int)$includeDeleted, null, Client::METHOD_GET);
        $headers = $response->getHeaders();
        $contentType = $headers['content-type'] ?? null;
        $rawContent = $response->getRawContent();
        return new \Symfony\Component\HttpFoundation\Response($rawContent, 200, ['Content-Type' => $contentType]);
    }

    /**
     * @inheritDoc
     */
    public function store(
        ?string $flowUuid,
        string $ownerPrefix,
        int $ownerId,
        DateTimeInterface $receivedDatetime,
        ?string $documentUuid = null,
        ?string $documentIdentifier = null,
        ?string $sender = null,
        ?string $receiver = null,
        ?string $receiverName = null,
        ?float $total = null,
        ?float $vatTotal = null,
        ?string $format = null,
        ?string $type = null,
        ?DateTimeInterface $downloadedDatetime = null,
        ?string $serviceTag = null,
        ?string $direction = null,
        ?DateTimeInterface $updatedDatetime = null,
        ?DateTimeInterface $createdDatetime = null
    ): Document
    {
        $data = [
            'flow_uuid' => $flowUuid,
            'document_uuid' => $documentUuid,
            'document_identifier' => $documentIdentifier,
            'owner_prefix' => $ownerPrefix,
            'owner_id' => $ownerId,
            'received_datetime' => is_null($receivedDatetime) ? $receivedDatetime : $receivedDatetime->format("Y-m-d H:i:s"),
            'sender' => $sender,
            'receiver' => $receiver,
            'receiver_name' => $receiverName,
            'total' => $total,
            'vat_total' => $vatTotal,
            'format' => $format,
            'type' => $type,
            'service_tag' => $serviceTag,
        ];
        if($downloadedDatetime) {
            $data['downloaded_datetime'] = $downloadedDatetime->format("Y-m-d H:i:s");
        }
        if($direction) {
            $data['direction'] = $direction;
        }
        if($updatedDatetime) {
            $data['updated_datetime'] = $updatedDatetime->format("Y-m-d H:i:s");
        }
        if($createdDatetime) {
            $data['created_datetime'] = $createdDatetime->format("Y-m-d H:i:s");
        }
        $response = $this->client->doRequest("documents", $data, Client::METHOD_POST);
        $record = $response->getResult();

        return $this->factory->make(
            $record->id,
            $record->serviceTag,
            $record->direction,
            $record->directionTranslated,
            $record->flowUuid,
            $record->documentUuid,
            $record->documentIdentifier,
            $record->status,
            $record->statusTranslated,
            $record->userStatus,
            $record->userStatusTranslated,
            $record->userAssignee,
            is_null($record->receivedDatetime) ? null : new \DateTime($record->receivedDatetime->date, new \DateTimeZone($record->receivedDatetime->timezone)),
            $record->sender,
            $record->receiver,
            $record->receiverName,
            $record->total,
            $record->vatTotal,
            $record->format,
            $record->type,
            $record->typeTranslated,
            $record->ownerPrefix,
            $record->ownerId,
            $record->lastLogEntryType,
            $record->lastLogEntryMessage,
            $record->lastLogEntryMessageTranslated,
            is_null($record->downloadedDatetime) ? null : new \DateTime($record->downloadedDatetime->date, new \DateTimeZone($record->downloadedDatetime->timezone)),
            is_null($record->updatedDatetime) ? null : new \DateTime($record->updatedDatetime->date, new \DateTimeZone($record->updatedDatetime->timezone)),
            is_null($record->createdDatetime) ? null : new \DateTime($record->createdDatetime->date, new \DateTimeZone($record->createdDatetime->timezone)),
            is_null($record->deletedDatetime) ? null : new \DateTime($record->deletedDatetime->date, new \DateTimeZone($record->deletedDatetime->timezone))
        );
    }

    /**
     * @inheritDoc
     */
    public function destroy(string $flowUuid, string $documentUuid): bool
    {
        $data = [
            'flow_uuid' => $flowUuid,
            'document_uuid' => $documentUuid
        ];
        $response = $this->client->doRequest("documents", $data, Client::METHOD_DELETE);
        return $response->getCode() == Response::CODE_OK;
    }

    /**
     * @inheritDoc
     */
    public function countByDateGrouped(DateTimeInterface $date, ?string $ownerPrefix = null, ?array $ownerIds = null, ?array $serviceTags = null, bool $includeDeleted = true): array
    {
        $data = [
            'date' => $date->format("Y-m-d"),
            'owner_prefix' => $ownerPrefix,
            'owner_ids' => $ownerIds,
            'service_tags' => $serviceTags,
            'include_deleted' => $includeDeleted
        ];
        $response = $this->client->doRequest('documents/count_by_date_grouped', $data, Client::METHOD_POST);
        return $response->getResult();
    }

    /**
     * @inheritDoc
     */
    public function countForInvoicing(
        string $ownerPrefix,
        string $ownerId,
        string $serviceTag,
        DateTimeInterface $from,
        DateTimeInterface $to,
        array $formats,
        array $types,
        array $directions,
        bool $includeDeleted = true
    ): int
    {
        $data = [
            'owner_prefix' => $ownerPrefix,
            'owner_id' => $ownerId,
            'service_tag' => $serviceTag,
            'from' => $from->format("Y-m-d"),
            'to' => $to->format("Y-m-d"),
            'formats' => $formats,
            'types' => $types,
            'directions' => $directions,
            'include_deleted' => $includeDeleted
        ];
        $response = $this->client->doRequest('documents/count_for_invoicing', $data, Client::METHOD_POST);
        return $response->getResult();
    }
}
