<?php

namespace Officient\DocumentList\Manager;

/**
 * Interface TranslationManagerInterface
 * @package Officient\DocumentList\Manager
 */
interface TranslationManagerInterface
{
    /**
     * @param string|null $locale
     * @return array
     */
    public function getMessages(?string $locale = null): array;

    /**
     * @param string|null $locale
     * @return array
     */
    public function getStatuses(?string $locale = null): array;

    /**
     * @param string|null $locale
     * @return array
     */
    public function getUserStatuses(?string $locale = null): array;

    /**
     * @param string|null $locale
     * @return array
     */
    public function getTypes(?string $locale = null): array;

    /**
     * @param string|null $locale
     * @return array
     */
    public function getDirections(?string $locale = null): array;
}