<?php

namespace Officient\DocumentList\Manager;

use Officient\DocumentList\Entity\Document;
use Officient\DocumentList\Entity\DocumentFile;
use Symfony\Component\HttpFoundation\Response;

interface DocumentFileManagerInterface
{
    /**
     * @param Document $document
     * @param array|null $criteria
     * @param int|null $limit
     * @param int|null $offset
     * @return int
     */
    public function countBy(Document $document, ?array $criteria = null, ?int $limit = null, ?int $offset = null): int;

    /**
     * @param Document $document
     * @param array|null $criteria
     * @param array|null $orderBy
     * @param int|null $limit
     * @param int|null $offset
     * @return DocumentFile[]
     */
    public function findBy(Document $document, ?array $criteria = null, array $orderBy = null, ?int $limit = null, ?int $offset = null): array;

    /**
     * @param Document $document
     * @param array $criteria
     * @param array|null $orderBy
     * @return DocumentFile|null
     */
    public function findOneBy(Document $document, array $criteria, array $orderBy = null): ?DocumentFile;

    /**
     * @param Document $document
     * @param string $tag
     * @return DocumentFile|null
     */
    public function findLatestOneByTag(Document $document, string $tag): ?DocumentFile;

    /**
     * @param Document|null $document
     * @param string|null $flowUuid
     * @param string|null $documentUuid
     * @param string $tag
     * @param bool $decode
     * @return string|null
     */
    public function findLatestContentByTag(?Document $document, ?string $flowUuid, ?string $documentUuid, string $tag, bool $decode = true): ?string;

    /**
     * @param Document $document
     * @param DocumentFile $documentFile
     * @param bool $decode
     * @return string
     */
    public function getContent(Document $document, DocumentFile $documentFile, bool $decode = true): ?string;

    /**
     * @param Document $document
     * @param DocumentFile $documentFile
     * @param bool $transform
     * @return Response
     */
    public function showContent(Document $document, DocumentFile $documentFile, bool $transform = false): Response;

    /**
     * @param Document $document
     * @param DocumentFile $documentFile
     * @return Response
     */
    public function downloadContent(Document $document, DocumentFile $documentFile): Response;

    /**
     * @param Document $document
     * @param string $data
     * @param string|null $tag
     * @param string|null $originalName
     * @param string|null $md5Checksum
     * @return DocumentFile
     */
    public function store(
        Document $document,
        string $data,
        ?string $tag = null,
        ?string $originalName = null,
        ?string $md5Checksum = null
    ): DocumentFile;
}