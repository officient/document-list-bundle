<?php

namespace Officient\DocumentList\Manager;

use Officient\DocumentList\Entity\Document;
use Officient\DocumentList\Entity\DownloadEntry;
use Officient\DocumentList\Entity\LogEntry;
use Symfony\Component\HttpFoundation\Response;

/**
 * Interface DownloadEntryManagerInterface
 * @package Officient\DocumentList\Manager
 */
interface DownloadEntryManagerInterface
{
    /**
     * @param array|null $criteria
     * @param int|null $limit
     * @param int|null $offset
     * @return int
     */
    public function countBy(?array $criteria = null, ?int $limit = null, ?int $offset = null): int;

    /**
     * @param array|null $criteria
     * @param array|null $orderBy
     * @param int|null $limit
     * @param int|null $offset
     * @return DownloadEntry[]
     */
    public function findBy(?array $criteria = null, array $orderBy = null, ?int $limit = null, ?int $offset = null): array;

    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @return DownloadEntry|null
     */
    public function findOneBy(array $criteria, array $orderBy = null): ?DownloadEntry;

    /**
     * @param string $ownerPrefix
     * @param int $ownerId
     * @param string $senderAddress
     * @param string $recipientAddress
     * @param string|null $senderName
     * @param string|null $recipientName
     * @param array|null $criteria
     * @param array|null $orderBy
     * @param int|null $limit
     * @param int|null $offset
     * @param bool $includeDeleted
     * @param string|null $locale
     * @return void
     */
    public function queue(
        string $ownerPrefix,
        int $ownerId,
        string $senderAddress,
        string $recipientAddress,
        ?string $senderName = null,
        ?string $recipientName = null,
        ?array $criteria = null,
        ?array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null,
        bool $includeDeleted = true,
        ?string $locale = null
    ): void;

    /**
     * @param DownloadEntry $downloadEntry
     * @param bool $base64Decode
     * @return string|false
     */
    public function getContent(DownloadEntry $downloadEntry, bool $base64Decode = true): string|false;

    /**
     * @param DownloadEntry $downloadEntry
     * @return Response
     */
    public function downloadContent(DownloadEntry $downloadEntry): Response;
}