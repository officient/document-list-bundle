<?php

namespace Officient\DocumentList;

use Officient\DocumentList\Exception\AlreadyExistsException;
use Officient\DocumentList\Exception\DocumentListException;
use Officient\DocumentList\Exception\NonUniqueResultException;
use Officient\DocumentList\Exception\NoResultException;
use Officient\DocumentList\Exception\NotFoundException;
use Officient\DocumentList\Exception\ValidationException;

/**
 * Class Response
 *
 * This class handles the Document List response data
 *
 * @package Officient\DocumentList
 */
class Response
{
    const CODE_OK                   = 1000;
    const CODE_NOT_FOUND            = 1001;
    const CODE_VALIDATION_ERROR     = 1002;
    const CODE_ALREADY_EXISTS       = 1003;
    const CODE_NON_UNIQUE_RESULT    = 1004;
    const CODE_NO_RESULT            = 1005;
    const CODE_EXCEPTION            = 1006;

    /**
     * @var mixed
     */
    private $rawContent;

    /**
     * @var object
     */
    private $content;

    /**
     * @var array|null
     */
    private $headers;

    /**
     * Response constructor.
     * @param string $rawContent
     * @param array|null $headers
     * @throws DocumentListException
     */
    public function __construct(string $rawContent, ?array $headers = null)
    {
        $this->rawContent = $rawContent;
        $this->content = json_decode($rawContent);
        $this->headers = $headers;
        $this->processCode();
    }

    /**
     * @throws DocumentListException
     */
    private function processCode()
    {
        switch($this->getCode()) {
            case self::CODE_NOT_FOUND:
                throw new NotFoundException($this->getMessage(), $this->getCode());
            case self::CODE_VALIDATION_ERROR:
                throw new ValidationException($this->getMessage(), $this->getCode(), json_decode(json_encode($this->getResult()), true));
            case self::CODE_ALREADY_EXISTS:
                throw new AlreadyExistsException($this->getMessage(), $this->getCode());
            case self::CODE_NON_UNIQUE_RESULT:
                throw new NonUniqueResultException($this->getMessage(), $this->getCode());
            case self::CODE_NO_RESULT:
                throw new NoResultException($this->getMessage(), $this->getCode());
            case self::CODE_EXCEPTION:
                $result = $this->getResult();
                $message = $this->getMessage()."\r\n";
                $message .= $result->namespace."\\".$result->exception."\r\n";
                $message .= $result->file." | Line ".$result->line."\r\n";
                $message .= $result->message;
                throw new DocumentListException($message, $this->getCode());
        }
    }

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return isset($this->content->message) ? $this->content->message : null;
    }

    /**
     * @return int|null
     */
    public function getCode(): ?int
    {
        return isset($this->content->code) ? $this->content->code : null;
    }

    /**
     * @return mixed|null
     */
    public function getResult()
    {
        return isset($this->content->result) ? $this->content->result : null;
    }

    /**
     * @return mixed
     */
    public function getRawContent()
    {
        return $this->rawContent;
    }

    /**
     * @return array|null
     */
    public function getHeaders(): ?array
    {
        return $this->headers;
    }
}