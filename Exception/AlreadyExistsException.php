<?php

namespace Officient\DocumentList\Exception;

/**
 * Class AlreadyExistsException
 * @package Officient\DocumentList\Exception
 */
class AlreadyExistsException extends DocumentListException
{

}