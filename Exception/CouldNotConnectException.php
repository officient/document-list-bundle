<?php

namespace Officient\DocumentList\Exception;

/**
 * Class CouldNotConnectException
 * @package Officient\DocumentList\Exception
 */
class CouldNotConnectException extends DocumentListException
{

}