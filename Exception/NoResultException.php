<?php

namespace Officient\DocumentList\Exception;

/**
 * Class NoResultException
 * @package Officient\DocumentList\Exception
 */
class NoResultException extends DocumentListException
{

}