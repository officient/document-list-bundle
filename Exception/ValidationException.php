<?php

namespace Officient\DocumentList\Exception;

use Throwable;

/**
 * Class ValidationException
 * @package Officient\DocumentList\Exception
 */
class ValidationException extends DocumentListException
{
    private array $violations;

    public function __construct(string $message = "", int $code = 0, array $violations = [], ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->violations = $violations;
    }

    public function getViolations(): array
    {
        return $this->violations;
    }
}