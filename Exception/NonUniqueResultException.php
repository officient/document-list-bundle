<?php

namespace Officient\DocumentList\Exception;

/**
 * Class NonUniqueResultException
 * @package Officient\DocumentList\Exception
 */
class NonUniqueResultException extends DocumentListException
{

}