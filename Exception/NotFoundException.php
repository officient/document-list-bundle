<?php

namespace Officient\DocumentList\Exception;

/**
 * Class NotFoundException
 * @package Officient\DocumentList\Exception
 */
class NotFoundException extends DocumentListException
{

}