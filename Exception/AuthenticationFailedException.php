<?php

namespace Officient\DocumentList\Exception;

/**
 * Class AuthenticationFailedException
 * @package Officient\DocumentList\Exception
 */
class AuthenticationFailedException extends DocumentListException
{

}