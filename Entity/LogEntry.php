<?php

namespace Officient\DocumentList\Entity;

use DateTimeInterface;

/**
 * Class LogEntry
 * @package Officient\DocumentList\Entity
 */
class LogEntry implements \JsonSerializable
{
    const TYPE_INFO             = 'info';
    const TYPE_USER_INFO        = 'user_info';
    const TYPE_ERROR            = 'error';
    const TYPE_DEBUG            = 'debug';

    const STATUS_OK             = 'ok';
    const STATUS_PROCESSING     = 'processing';
    const STATUS_ERROR          = 'error';

    const USER_STATUS_PENDING   = 'pending';
    const USER_STATUS_HANDLED   = 'handled';

    /**
     * @var int|null
     */
    private $id;

    /**
     * @var string|null
     */
    private $message;

    /**
     * @var string|null
     */
    private $messageTranslated;

    /**
     * @var string|null
     */
    private $type;

    /**
     * @var array|null
     */
    private $payload;

    /**
     * @var string|null
     */
    private $status;

    /**
     * @var string|null
     */
    private $statusTranslated;

    /**
     * @var string|null
     */
    private $userStatus;

    /**
     * @var string|null
     */
    private $userStatusTranslated;

    /**
     * @var int|null
     */
    private $userAssignee;

    /**
     * @var bool|null
     */
    private $muted;

    /**
     * @var DateTimeInterface|null
     */
    private $createdDatetime;

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): mixed
    {
        return get_object_vars($this);
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return LogEntry
     */
    public function setId(?int $id): LogEntry
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string|null $message
     * @return LogEntry
     */
    public function setMessage(?string $message): LogEntry
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMessageTranslated(): ?string
    {
        return $this->messageTranslated;
    }

    /**
     * @param string|null $messageTranslated
     * @return LogEntry
     */
    public function setMessageTranslated(?string $messageTranslated): LogEntry
    {
        $this->messageTranslated = $messageTranslated;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     * @return LogEntry
     */
    public function setType(?string $type): LogEntry
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getPayload(): ?array
    {
        return $this->payload;
    }

    /**
     * @param array|null $payload
     * @return LogEntry
     */
    public function setPayload(?array $payload): LogEntry
    {
        $this->payload = $payload;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string|null $status
     * @return $this
     */
    public function setStatus(?string $status): LogEntry
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatusTranslated(): ?string
    {
        return $this->statusTranslated;
    }

    /**
     * @param string|null $statusTranslated
     * @return LogEntry
     */
    public function setStatusTranslated(?string $statusTranslated): LogEntry
    {
        $this->statusTranslated = $statusTranslated;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUserStatus(): ?string
    {
        return $this->userStatus;
    }

    /**
     * @param string|null $userStatus
     * @return LogEntry
     */
    public function setUserStatus(?string $userStatus): LogEntry
    {
        $this->userStatus = $userStatus;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUserStatusTranslated(): ?string
    {
        return $this->userStatusTranslated;
    }

    /**
     * @param string|null $userStatusTranslated
     * @return LogEntry
     */
    public function setUserStatusTranslated(?string $userStatusTranslated): LogEntry
    {
        $this->userStatusTranslated = $userStatusTranslated;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserAssignee(): ?int
    {
        return $this->userAssignee;
    }

    /**
     * @param int|null $userAssignee
     * @return LogEntry
     */
    public function setUserAssignee(?int $userAssignee): LogEntry
    {
        $this->userAssignee = $userAssignee;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getMuted(): ?bool
    {
        return $this->muted;
    }

    /**
     * @param bool|null $muted
     * @return LogEntry
     */
    public function setMuted(?bool $muted): LogEntry
    {
        $this->muted = $muted;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getCreatedDatetime(): ?DateTimeInterface
    {
        return $this->createdDatetime;
    }

    /**
     * @param DateTimeInterface|null $createdDatetime
     * @return LogEntry
     */
    public function setCreatedDatetime(?DateTimeInterface $createdDatetime): LogEntry
    {
        $this->createdDatetime = $createdDatetime;
        return $this;
    }
}