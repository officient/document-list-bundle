<?php

namespace Officient\DocumentList\Entity;

use DateTimeInterface;

/**
 * Class Document
 * @package Officient\DocumentList\Entity
 */
class Document implements \JsonSerializable
{
    const FORMAT_EHF                    = 'ehf';
    const FORMAT_OIOUBL                 = 'oioubl';
    const FORMAT_PEPPOL                 = 'peppol';
    const FORMAT_OIOXML                 = 'oioxml';
    const FORMAT_XRECHNUNG              = 'xrechnung';
    const FORMAT_EFACTO                 = 'efacto';
    const FORMAT_ABWINVOICE             = 'abwinvoice';
    const FORMAT_COOR                   = 'coor';
    const FORMAT_CXML                   = 'cxml';
    const FORMAT_E2B                    = 'e2b';
    const FORMAT_ECXML                  = 'ecxml';
    const FORMAT_EDIFACT                = 'edifact';
    const FORMAT_EYESHARE_SCAN          = 'eyeshare_scan';
    const FORMAT_finvoice               = 'finvoice';
    const FORMAT_FIREWALL               = 'firewall';
    const FORMAT_GRIEG_SCAN             = 'grieg_scan';
    const FORMAT_INTEGRATION            = 'integration';
    const FORMAT_INTERFACE              = 'interface';
    const FORMAT_NORDEA_SUPPLIER_FILE   = 'nordea_supplier_file';
    const FORMAT_PDF                    = 'pdf';
    const FORMAT_PROOUTRESP             = 'prooutresp';
    const FORMAT_SVE1                   = 'sve1';
    const FORMAT_SVEFAK                 = 'svefak';

    const TYPE_INVOICE                  = 'invoice';
    const TYPE_CREDITNOTE               = 'creditnote';
    const TYPE_CREDIT_NOTE              = 'credit_note';
    const TYPE_ABNORMALITY              = 'abnormality';
    const TYPE_REMINDER                 = 'reminder';
    const TYPE_UTILITY_STATEMENT        = 'utility_statement';
    const TYPE_UTS                      = 'uts';
    const TYPE_APPLICATION_RESPONSE     = 'application_response';
    const TYPE_CATALOGUE                = 'catalogue';
    CONST TYPE_ORDER                    = 'order';
    CONST TYPE_ORDER_RESPONSE           = 'order_response';
    CONST TYPE_SCAN_FILE                = 'scan_file';
    CONST TYPE_DOCUMENT                 = 'document';

    const DIRECTION_SENT                = 'sent';
    const DIRECTION_RECEIVED            = 'received';

    /**
     * @var int|null
     */
    private $id;

    /**
     * @var string|null
     */
    private $serviceTag;

    /**
     * @var string|null
     */
    private $direction;

    /**
     * @var string|null
     */
    private $directionTranslated;

    /**
     * @var string|null
     */
    private $flowUuid;

    /**
     * @var string|null
     */
    private $documentUuid;

    /**
     * @var string|null
     */
    private $documentIdentifier;

    /**
     * @var string|null
     */
    private $status;

    /**
     * @var string|null
     */
    private $statusTranslated;

    /**
     * @var string|null
     */
    private $userStatus;

    /**
     * @var string|null
     */
    private $userStatusTranslated;

    /**
     * @var int|null
     */
    private $userAssignee;

    /**
     * @var DateTimeInterface|null
     */
    private $receivedDatetime;

    /**
     * @var string|null
     */
    private $sender;

    /**
     * @var string|null
     */
    private $receiver;

    /**
     * @var string|null
     */
    private $receiverName;

    /**
     * @var float|null
     */
    private $total;

    /**
     * @var float|null
     */
    private $vatTotal;

    /**
     * @var string|null
     */
    private $format;

    /**
     * @var string|null
     */
    private $type;

    /**
     * @var string|null
     */
    private $typeTranslated;

    /**
     * @var string|null
     */
    private $ownerPrefix;

    /**
     * @var int|null
     */
    private $ownerId;

    /**
     * @var string|null
     */
    private $lastLogEntryType;

    /**
     * @var string|null
     */
    private $lastLogEntryMessage;

    /**
     * @var string|null
     */
    private $lastLogEntryMessageTranslated;

    /**
     * @var DateTimeInterface|null
     */
    private $downloadedDatetime;

    /**
     * @var DateTimeInterface|null
     */
    private $updatedDatetime;

    /**
     * @var DateTimeInterface|null
     */
    private $createdDatetime;

    /**
     * @var DateTimeInterface|null
     */
    private $deletedDatetime;

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): mixed
    {
        return get_object_vars($this);
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Document
     */
    public function setId(?int $id): Document
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getServiceTag(): ?string
    {
        return $this->serviceTag;
    }

    /**
     * @param string|null $serviceTag
     * @return Document
     */
    public function setServiceTag(?string $serviceTag): Document
    {
        $this->serviceTag = $serviceTag;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDirection(): ?string
    {
        return $this->direction;
    }

    /**
     * @param string|null $direction
     */
    public function setDirection(?string $direction): void
    {
        $this->direction = $direction;
    }

    /**
     * @return string|null
     */
    public function getDirectionTranslated(): ?string
    {
        return $this->directionTranslated;
    }

    /**
     * @param string|null $directionTranslated
     */
    public function setDirectionTranslated(?string $directionTranslated): void
    {
        $this->directionTranslated = $directionTranslated;
    }

    /**
     * @return string|null
     */
    public function getFlowUuid(): ?string
    {
        return $this->flowUuid;
    }

    /**
     * @param string|null $flowUuid
     * @return Document
     */
    public function setFlowUuid(?string $flowUuid): Document
    {
        $this->flowUuid = $flowUuid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDocumentUuid(): ?string
    {
        return $this->documentUuid;
    }

    /**
     * @param string|null $documentUuid
     * @return Document
     */
    public function setDocumentUuid(?string $documentUuid): Document
    {
        $this->documentUuid = $documentUuid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDocumentIdentifier(): ?string
    {
        return $this->documentIdentifier;
    }

    /**
     * @param string|null $documentIdentifier
     * @return Document
     */
    public function setDocumentIdentifier(?string $documentIdentifier): Document
    {
        $this->documentIdentifier = $documentIdentifier;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string|null $status
     * @return Document
     */
    public function setStatus(?string $status): Document
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatusTranslated(): ?string
    {
        return $this->statusTranslated;
    }

    public function setStatusTranslated(?string $statusTranslated): Document
    {
        $this->statusTranslated = $statusTranslated;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUserStatus(): ?string
    {
        return $this->userStatus;
    }

    /**
     * @param string|null $userStatus
     * @return Document
     */
    public function setUserStatus(?string $userStatus): Document
    {
        $this->userStatus = $userStatus;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUserStatusTranslated(): ?string
    {
        return $this->userStatusTranslated;
    }

    /**
     * @param string|null $userStatusTranslated
     * @return Document
     */
    public function setUserStatusTranslated(?string $userStatusTranslated): Document
    {
        $this->userStatusTranslated = $userStatusTranslated;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserAssignee(): ?int
    {
        return $this->userAssignee;
    }

    /**
     * @param int|null $userAssignee
     * @return Document
     */
    public function setUserAssignee(?int $userAssignee): Document
    {
        $this->userAssignee = $userAssignee;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getReceivedDatetime(): ?DateTimeInterface
    {
        return $this->receivedDatetime;
    }

    /**
     * @param DateTimeInterface|null $receivedDatetime
     * @return Document
     */
    public function setReceivedDatetime(?DateTimeInterface $receivedDatetime): Document
    {
        $this->receivedDatetime = $receivedDatetime;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSender(): ?string
    {
        return $this->sender;
    }

    /**
     * @param string|null $sender
     * @return Document
     */
    public function setSender(?string $sender): Document
    {
        $this->sender = $sender;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getReceiver(): ?string
    {
        return $this->receiver;
    }

    /**
     * @param string|null $receiver
     * @return Document
     */
    public function setReceiver(?string $receiver): Document
    {
        $this->receiver = $receiver;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getReceiverName(): ?string
    {
        return $this->receiverName;
    }

    /**
     * @param string|null $receiverName
     * @return Document
     */
    public function setReceiverName(?string $receiverName): Document
    {
        $this->receiverName = $receiverName;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getTotal(): ?float
    {
        return $this->total;
    }

    /**
     * @param float|null $total
     * @return Document
     */
    public function setTotal(?float $total): Document
    {
        $this->total = $total;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getVatTotal(): ?float
    {
        return $this->vatTotal;
    }

    /**
     * @param float|null $vatTotal
     * @return Document
     */
    public function setVatTotal(?float $vatTotal): Document
    {
        $this->vatTotal = $vatTotal;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFormat(): ?string
    {
        return $this->format;
    }

    /**
     * @param string|null $format
     * @return Document
     */
    public function setFormat(?string $format): Document
    {
        $this->format = $format;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     * @return Document
     */
    public function setType(?string $type): Document
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTypeTranslated(): ?string
    {
        return $this->typeTranslated;
    }

    /**
     * @param string|null $typeTranslated
     * @return Document
     */
    public function setTypeTranslated(?string $typeTranslated): Document
    {
        $this->typeTranslated = $typeTranslated;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOwnerPrefix(): ?string
    {
        return $this->ownerPrefix;
    }

    /**
     * @param string|null $ownerPrefix
     * @return Document
     */
    public function setOwnerPrefix(?string $ownerPrefix): Document
    {
        $this->ownerPrefix = $ownerPrefix;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getOwnerId(): ?int
    {
        return $this->ownerId;
    }

    /**
     * @param int|null $ownerId
     * @return Document
     */
    public function setOwnerId(?int $ownerId): Document
    {
        $this->ownerId = $ownerId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastLogEntryType(): ?string
    {
        return $this->lastLogEntryType;
    }

    /**
     * @param string|null $lastLogEntryType
     * @return Document
     */
    public function setLastLogEntryType(?string $lastLogEntryType): Document
    {
        $this->lastLogEntryType = $lastLogEntryType;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastLogEntryMessage(): ?string
    {
        return $this->lastLogEntryMessage;
    }

    /**
     * @param string|null $lastLogEntryMessage
     * @return Document
     */
    public function setLastLogEntryMessage(?string $lastLogEntryMessage): Document
    {
        $this->lastLogEntryMessage = $lastLogEntryMessage;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastLogEntryMessageTranslated(): ?string
    {
        return $this->lastLogEntryMessageTranslated;
    }

    /**
     * @param string|null $lastLogEntryMessageTranslated
     * @return Document
     */
    public function setLastLogEntryMessageTranslated(?string $lastLogEntryMessageTranslated): Document
    {
        $this->lastLogEntryMessageTranslated = $lastLogEntryMessageTranslated;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getDownloadedDatetime(): ?DateTimeInterface
    {
        return $this->downloadedDatetime;
    }

    /**
     * @param DateTimeInterface|null $downloadedDatetime
     * @return Document
     */
    public function setDownloadedDatetime(?DateTimeInterface $downloadedDatetime): Document
    {
        $this->downloadedDatetime = $downloadedDatetime;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getUpdatedDatetime(): ?DateTimeInterface
    {
        return $this->updatedDatetime;
    }

    /**
     * @param DateTimeInterface|null $updatedDatetime
     * @return Document
     */
    public function setUpdatedDatetime(?DateTimeInterface $updatedDatetime): Document
    {
        $this->updatedDatetime = $updatedDatetime;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getCreatedDatetime(): ?DateTimeInterface
    {
        return $this->createdDatetime;
    }

    /**
     * @param DateTimeInterface|null $createdDatetime
     * @return Document
     */
    public function setCreatedDatetime(?DateTimeInterface $createdDatetime): Document
    {
        $this->createdDatetime = $createdDatetime;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getDeletedDatetime(): ?DateTimeInterface
    {
        return $this->deletedDatetime;
    }

    /**
     * @param DateTimeInterface|null $deletedDatetime
     * @return Document
     */
    public function setDeletedDatetime(?DateTimeInterface $deletedDatetime): Document
    {
        $this->deletedDatetime = $deletedDatetime;
        return $this;
    }
}