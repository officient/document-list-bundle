<?php

namespace Officient\DocumentList\Entity;

use DateTimeInterface;

/**
 * Class DownloadEntry
 * @package Officient\DocumentList\Entity
 */
class DownloadEntry implements \JsonSerializable
{
    /**
     * @var int|null
     */
    private $id;

    /**
     * @var string|null
     */
    private $ownerPrefix;

    /**
     * @var int|null
     */
    private $ownerId;

    /**
     * @var string|null
     */
    private $uuid;

    /**
     * @var string|null
     */
    private $fileName;

    /**
     * @var DateTimeInterface|null
     */
    private $expiresDatetime;

    /**
     * @var DateTimeInterface|null
     */
    private $createdDatetime;


    /**
     * @inheritDoc
     */
    public function jsonSerialize(): mixed
    {
        return get_object_vars($this);
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return DownloadEntry
     */
    public function setId(?int $id): DownloadEntry
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOwnerPrefix(): ?string
    {
        return $this->ownerPrefix;
    }

    /**
     * @param string|null $ownerPrefix
     * @return DownloadEntry
     */
    public function setOwnerPrefix(?string $ownerPrefix): DownloadEntry
    {
        $this->ownerPrefix = $ownerPrefix;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getOwnerId(): ?int
    {
        return $this->ownerId;
    }

    /**
     * @param int|null $ownerId
     * @return DownloadEntry
     */
    public function setOwnerId(?int $ownerId): DownloadEntry
    {
        $this->ownerId = $ownerId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    /**
     * @param string|null $uuid
     * @return DownloadEntry
     */
    public function setUuid(?string $uuid): DownloadEntry
    {
        $this->uuid = $uuid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    /**
     * @param string|null $fileName
     * @return DownloadEntry
     */
    public function setFileName(?string $fileName): DownloadEntry
    {
        $this->fileName = $fileName;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getExpiresDatetime(): ?DateTimeInterface
    {
        return $this->expiresDatetime;
    }

    /**
     * @param DateTimeInterface|null $expiresDatetime
     * @return DownloadEntry
     */
    public function setExpiresDatetime(?DateTimeInterface $expiresDatetime): DownloadEntry
    {
        $this->expiresDatetime = $expiresDatetime;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getCreatedDatetime(): ?DateTimeInterface
    {
        return $this->createdDatetime;
    }

    /**
     * @param DateTimeInterface|null $createdDatetime
     * @return DownloadEntry
     */
    public function setCreatedDatetime(?DateTimeInterface $createdDatetime): DownloadEntry
    {
        $this->createdDatetime = $createdDatetime;
        return $this;
    }
}