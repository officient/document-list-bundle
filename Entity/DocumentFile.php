<?php

namespace Officient\DocumentList\Entity;

class DocumentFile implements \JsonSerializable
{
    /**
     * @var int|null
     */
    private $id;

    /**
     * @var string|null
     */
    private $tag;

    /**
     * @var string|null
     */
    private $mimeType;

    /**
     * @var string|null
     */
    private $originalName;

    /**
     * @var string|null
     */
    private $diskReference;

    /**
     * @var \DateTimeInterface|null
     */
    private $createdDatetime;

    public function jsonSerialize(): mixed
    {
        return get_object_vars($this);
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return DocumentFile
     */
    public function setId(?int $id): DocumentFile
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTag(): ?string
    {
        return $this->tag;
    }

    /**
     * @param string|null $tag
     * @return DocumentFile
     */
    public function setTag(?string $tag): DocumentFile
    {
        $this->tag = $tag;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMimeType(): ?string
    {
        return $this->mimeType;
    }

    /**
     * @param string|null $mimeType
     * @return DocumentFile
     */
    public function setMimeType(?string $mimeType): DocumentFile
    {
        $this->mimeType = $mimeType;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOriginalName(): ?string
    {
        return $this->originalName;
    }

    /**
     * @param string|null $originalName
     * @return DocumentFile
     */
    public function setOriginalName(?string $originalName): DocumentFile
    {
        $this->originalName = $originalName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDiskReference(): ?string
    {
        return $this->diskReference;
    }

    /**
     * @param string|null $diskReference
     * @return DocumentFile
     */
    public function setDiskReference(?string $diskReference): DocumentFile
    {
        $this->diskReference = $diskReference;
        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedDatetime(): ?\DateTimeInterface
    {
        return $this->createdDatetime;
    }

    /**
     * @param \DateTimeInterface|null $createdDatetime
     * @return DocumentFile
     */
    public function setCreatedDatetime(?\DateTimeInterface $createdDatetime): DocumentFile
    {
        $this->createdDatetime = $createdDatetime;
        return $this;
    }
}