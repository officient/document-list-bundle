<?php

namespace Officient\DocumentList\Command;

use Officient\DocumentList\LogEntry;
use Officient\DocumentList\Manager\DocumentManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;

class GetOneCommand extends Command
{
    /**
     * @var DocumentManagerInterface
     */
    private $documentManager;

    /**
     * @inheritDoc
     */
    public function __construct(DocumentManagerInterface $documentManager)
    {
        parent::__construct(null);
        $this->documentManager = $documentManager;
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('doc:getone')
            ->setDescription('Get one document')
            ->addArgument('id', InputArgument::REQUIRED, 'Document id');
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $id = $input->getArgument('id');

        $document = $this->documentManager->findOneBy([
            'id' => $id
        ]);

        if($document) {
            $columns = ['Id', 'Flow UUID', 'Doc UUID', 'Identifier', 'Company id', 'Type', 'Format', 'Sender', 'Receiver', 'Att count', 'Created', 'Received'];
            $rows = [[
                $document->getId(),
                $document->getFlowUuid(),
                $document->getDocumentUuid(),
                $document->getDocumentIdentifier(),
                $document->getCompanyId(),
                $document->getType(),
                $document->getFormat(),
                $document->getSender(),
                $document->getReceiver(),
                (string)count($document->getAttachments()),
                $document->getCreatedDatetime()->format('d-m-Y H:i:s'),
                $document->getReceivedDatetime()->format('d-m-Y H:i:s')
            ]];

            $io->table($columns, $rows);

            $choices = [
                'Original',
                'Html',
                'Logs',
                'Quit'
            ];

            $question = new ChoiceQuestion('Print extra stuff?', $choices);
            if($answer = strtoupper($io->askQuestion($question))) {
                switch ($answer) {
                    case 'ORIGINAL':
                        $io->writeln($document->getOriginal()->getMimeType());
                        $io->writeln($document->getOriginal()->getContent());
                        break;
                    case 'HTML':
                        $io->writeln($document->getHtml());
                        break;
                    case 'LOGS':
                        $logColumns = ['Type', 'Message', 'Timestamp'];
                        $logRows = array_map(function(LogEntry $log) {
                            return [
                                $log->getType(),
                                $log->getMessage(),
                                $log->getTimestamp()->format('d-m-Y H:i:s')
                            ];
                        }, $document->getLogEntries());
                        $io->table($logColumns, $logRows);
                        break;
                }
            }
        } else {
            $io->error("Document with id $id not found");
        }

        return 0;
    }
}