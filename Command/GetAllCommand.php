<?php

namespace Officient\DocumentList\Command;

use Officient\DocumentList\Entity\Document;
use Officient\DocumentList\LogEntry;
use Officient\DocumentList\Manager\DocumentManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;

class GetAllCommand extends Command
{
    /**
     * @var DocumentManagerInterface
     */
    private $documentManager;

    /**
     * @inheritDoc
     */
    public function __construct(DocumentManagerInterface $documentManager)
    {
        parent::__construct(null);
        $this->documentManager = $documentManager;
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('doc:all')
            ->setDescription('Get a list of all documents')
            ->addArgument('company_id', InputArgument::OPTIONAL, 'Filter on company id');
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $companyId = $input->getArgument('company_id');
        if($companyId) {
            $documents = $this->documentManager->findBy([
                'companyId' => $companyId
            ]);
        } else {
            $documents = $this->documentManager->findBy();
        }


        $columns = ['Id', 'Flow UUID', 'Doc UUID', 'Identifier', 'Company id', 'Created', 'Received'];
        $rows = array_map(function(Document $document) {
            return [
                $document->getId(),
                $document->getFlowUuid(),
                $document->getDocumentUuid(),
                $document->getDocumentIdentifier(),
                $document->getCompanyId(),
                $document->getCreatedDatetime()->format('d-m-Y H:i:s'),
                $document->getReceivedDatetime()->format('d-m-Y H:i:s')
            ];
        }, $documents);

        $io->table($columns, $rows);

        return 0;
    }
}