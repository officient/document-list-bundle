<?php

namespace Officient\DocumentList;

/**
 * Class Attachment
 * @package Officient\DocumentList
 */
class Attachment implements \JsonSerializable
{
    /**
     * @var string|null
     */
    private $fileName;

    /**
     * @var string|null
     */
    private $fileExtension;

    /**
     * @var string|null
     */
    private $content;

    /**
     * @var string|null
     */
    private $mimeType;

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): mixed
    {
        return get_object_vars($this);
    }

    /**
     * @return string|null
     */
    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    /**
     * @param string|null $fileName
     */
    public function setFileName(?string $fileName): void
    {
        $this->fileName = $fileName;
    }

    /**
     * @return string|null
     */
    public function getFileExtension(): ?string
    {
        return $this->fileExtension;
    }

    /**
     * @param string|null $fileExtension
     */
    public function setFileExtension(?string $fileExtension): void
    {
        $this->fileExtension = $fileExtension;
    }

    /**
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string|null $content
     * @return Attachment
     */
    public function setContent(?string $content): Attachment
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMimeType(): ?string
    {
        return $this->mimeType;
    }

    /**
     * @param string|null $mimeType
     * @return Attachment
     */
    public function setMimeType(?string $mimeType): Attachment
    {
        $this->mimeType = $mimeType;
        return $this;
    }
}