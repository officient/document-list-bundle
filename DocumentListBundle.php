<?php

namespace Officient\DocumentList;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class DocumentListBundle
 *
 * This class just needs to be here, because of stuff
 * in symfony documentation...
 *
 * @package Officient\DocumentList
 */
class DocumentListBundle extends Bundle
{

}